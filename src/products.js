import product1 from "./assets/products/ABcCap.jpg";
import product2 from "./assets/products/AirJordanShoe.jpg";
import product3 from "./assets/products/Cargopants.jpg";
import product4 from "./assets/products/DieselWatch.jpg";
import product5 from "./assets/products/DrxzCap.jpg";
import product6 from "./assets/products/FitnessShirt.jpg";
import product7 from "./assets/products/NikeShoe.jpg";
import product8 from "./assets/products/ResistorShirt.jpg";

export const PRODUCTS = [
  {
    id: 1,
    productName: "ABC",
    price:"12",
    productImage: product1,
  },
  {
    id: 2,
    productName: "Air Jordan",
    price: 19.0,
    productImage: product2,
  },
  {
    id: 3,
    productName: "Cargo Pant",
    price: 40.0,
    productImage: product3,
  },
  {
    id: 4,
    productName: "Diesel Watch",
    price: 21.0,
    productImage: product4,
  },
  {
    id: 5,
    productName: "D R X Cap",
    price: 15.99,
    productImage: product5,
  },
  {
    id: 6,
    productName: "Fitness Shirt",
    price: 26.0,
    productImage: product6,
  },
  {
    id: 7,
    productName: "N I K E shoe",
    price: 28.0,
    productImage: product7,
  },
  {
    id: 8,
    productName: "Resistore Shirt",
    price: 22.0,
    productImage: product8,
  },
];
